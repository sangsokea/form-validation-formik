import FileUploadForm from "@/components/FileUploadForm";
import React from "react";

export default function upload() {
  return (
    <div className="container d-flex justify-content-center my-5">
      <div>
        <h3 className="my-3">File Validation</h3>
        <FileUploadForm />
      </div>
    </div>
  );
}
