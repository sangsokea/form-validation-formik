import React from "react";
import DataTable from "react-data-table-component";

export default function Home({ data }) {
  const [filterProduct, setFilterProduct] = React.useState(data);
  const [products, setProducts] = React.useState(data);
  const [search, setSearch] = React.useState("");
  const [pending, setPending] = React.useState(true);
  const [isEdit, setIsEdit] = React.useState(false);

  // fetch data
  React.useEffect(() => {
    setTimeout(() => {
      fetch("https://api.escuelajs.co/api/v1/products")
        .then((res) => res.json())
        .then((data) => {
          setPending(false);
          setProducts(data);
          setFilterProduct(data);
        });
    }, [1000]);
  }, []);

  // filter data
  React.useEffect(() => {
    if (search.trim() !== "") {
      const result = products.filter((product) => {
        return `${product.title}`.toLowerCase().includes(search.toLowerCase());
      });
      setFilterProduct(result);
    }
  }, [search]);

  const columns = [
    {
      name: "Id", // name of the column
      selector: (row) => row.id, // selector function that returns the cell data for the column
    },
    {
      name: "Title",
      selector: (row) => <span>{row.title}</span>,
      sortable: true,
    },
    {
      name: "Price",
      selector: (row) => <span className="text-danger">${row.price}</span>,
      sortable: true,
    },
    // {
    //   name: "Description",
    //   selector: (row) => row.description,
    // },
    {
      name: "Category",
      selector: (row) => row.category.name,
    },
    {
      name: "Image",
      selector: (row) => <img width={80} height={80} src={row.images[0]} />,
    },
    {
      name: "Actions",
      selector: (row) => (
        <div>
          <button type="button" class="btn btn-link btn-sm btn-rounded">
            Edit
          </button>
          <button
            type="button"
            class="btn btn-link btn-sm btn-rounded text-danger"
          >
            Delete
          </button>
        </div>
      ),
    },
  ];

  return (
    <>
      <main className="container my-5 z-1">
        <DataTable
          progressPending={pending}
          customStyles={customStyles}
          // conditionalRowStyles={conditionalRowStyles}
          columns={columns}
          data={filterProduct}
          pagination
          responsive
          subHeaderAlign="right"
          highlightOnHover
          pointerOnHover
          selectableRows
          subHeader // adding subHeader to enable search
          subHeaderComponent={
            <div className="d-flex justify-content-between w-100 ">
              <h1>Products Lists</h1>
              <input
                style={{ width: "300px" }}
                type="text"
                placeholder="Search"
                className="form-control"
                value={search}
                onChange={(e) => {
                  const value = e.target.value;
                  setSearch(value);
                }}
              />
            </div>
          }
        />
      </main>
      <button
        type="button"
        class="btn btn-primary"
        data-mdb-toggle="modal"
        data-mdb-target="#exampleModal"
      >
        Launch demo modal
      </button>
      <div
        class="modal fade"
        id="exampleModal"
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">
                Modal title
              </h5>
              <button
                type="button"
                class="btn-close"
                data-mdb-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div class="modal-body">...</div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-mdb-dismiss="modal"
              >
                Close
              </button>
              <button type="button" class="btn btn-primary">
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

//  Internally, customStyles will deep merges your customStyles with the default styling.
const customStyles = {
  rows: {
    style: {
      minHeight: "72px", // override the row height
    },
  },
  rows: {
    style: {
      minHeight: "72px", // override the row height
    },
  },
  headCells: {
    style: {
      paddingLeft: "8px", // override the cell padding for head cells
      paddingRight: "8px",
      backgroundColor: "#F8F8F8",
    },
  },
  cells: {
    style: {
      paddingLeft: "8px", // override the cell padding for data cells
      paddingRight: "8px",
    },
  },
};

// const conditionalRowStyles = [
//   {
//     when: (row) => (row.id)%2 === 0,
//     style: {
//       backgroundColor: "green",
//       color: "white",
//       "&:hover": {
//         cursor: "pointer",
//       },
//     },
//   },
// // You can also pass a callback to style for additional customization
// {
//   when: (row) => row.calories < 400,
//   style: (row) => ({ backgroundColor: row.isSpecial ? "pink" : "inerit" }),
// },
// ];
