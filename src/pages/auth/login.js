"client";

import FormComponent from "@/components/FormComponent";
import React from "react";

export default function login() {
  return (
    <div
      style={{ height: "100vh" }}
      className="w-100 d-flex justify-content-center align-items-center"
    >
      
      <div className="w-25">
      <h1>Login</h1>
        <FormComponent />
        
      </div>

    </div>
  );
}
