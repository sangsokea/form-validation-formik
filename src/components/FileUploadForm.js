import React, { useEffect, useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import axios from "axios";

const FILE_SIZE = 1024 * 1024 * 2; // 2MB
const SUPPORTED_FORMATS = [
  "image/jpg",
  "image/jpeg",
  "image/png",
  "application/pdf",
];

const FileUploadSchema = Yup.object().shape({
  files: Yup.mixed() // mixed() is used to validate file type
    // test() is used to test the file size and file format
    // test() takes two arguments, first one is the name of the test and second one is the error message
    // test() returns true if the validation is successful and returns false if the validation fails
    .test("fileSize", "File too large", (value) => {
      if (!value) {
        // Return true if the field is empty
        return true;
      }
      return value.size <= FILE_SIZE;
    })
    .test("fileFormat", "Unsupported Format", (value) => {
      if (!value) {
        // Return true if the field is empty
        return true;
      }
      return SUPPORTED_FORMATS.includes(value.type);
    })
    .required("Required"),
});

const FileUploadForm = () => {
  const handleSubmitToServer = async (values) => {
    try {
      // axios is used to make HTTP requests to the server
      const response = await axios.post(
        "http://localhost:8083/api/v1/file/file-upload",
        values.files
      );
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <>
      <Formik
        initialValues={{ files: undefined }}
        validationSchema={FileUploadSchema}
        onSubmit={(values) => {
          // FormData is used to create a form data object
          const formData = new FormData();
          // append() is used to append values to the FormData object
          formData.append("file", values.files);

          // upload valid files to the server
          handleSubmitToServer({ files: formData });
        }}
      >
        {({ setFieldValue }) => (
          <Form>
            <Field
              name="files"
              type="file"
              title="Select a file"
              setFieldValue={setFieldValue} // Set Formik value
              component={CustomInput} // component prop is used to render the custom input
            />
            <ErrorMessage name="files">
              {(msg) => <div className="text-danger">{msg}</div>}
            </ErrorMessage>
            <button type="submit">Submit</button>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default FileUploadForm;

// field, form, setFieldValue are props passed by Formik
export const CustomInput = ({ field, form, setFieldValue, ...props }) => {
  const [imagePreview, setImagePreview] = useState(null);

  // this function is used handle the file selection
  const handleChange = (event) => {
    event.preventDefault();
    const file = event.currentTarget.files[0];
    // call setFieldValue and pass the field name and file object to it
    setFieldValue(field.name, file);
    // URL.createObjectURL() converts the selected file into a URL which can be used to display preview of the selected file
    setImagePreview(URL.createObjectURL(file));
  };
  return (
    <>
      <input
        type="file"
        onChange={(event) => {
          handleChange(event);
        }}
        {...props}
      />
      {imagePreview && (
        <img src={imagePreview} alt="preview" style={{ height: "100px" }} />
      )}
    </>
  );
};
