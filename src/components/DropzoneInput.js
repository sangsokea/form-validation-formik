import React from "react";
import { useField } from "formik";
import { useDropzone } from "react-dropzone";

export const DropzoneInput = (props) => {
  // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
  // which we can spread on <input> and also replace ErrorMessage entirely.
  const [field, meta, helpers] = useField({
    name: "myField",
    type: "text",
    required: true,
  });

  // useDropzone() returns an object with several properties and methods
  // we can use to handle the drag and drop functionality
  const { getRootProps, getInputProps, isDragActive, acceptedFiles } =
    useDropzone({
      accept: props.accept, // pass the accept prop to specify the MIME types to accept
      onDrop: (files) => {
        helpers.setValue(files); // set the field value to the selected files
      },
    });

  // render the files that are accepted by useDropzone
  const files = acceptedFiles.map((file) => (
    <li key={file.name}>
      {file.name} - {file.size} bytes
      <button onClick={() => helpers.setValue([])}>Remove</button> // remove the
      file by setting the field value to an empty array
    </li>
  ));

  return (
    <>
      <div {...getRootProps()}>
        <input {...field} {...getInputProps()} />
        {isDragActive ? (
          <p className="bg-primary">Drop the files here ...</p>
        ) : (
          <p className="bg-primary p-3">
            Drag and drop some files here, or click to select files
          </p>
        )}
      </div>
      <ul>{files}</ul>
    </>
  );
};
